# Índice

 * [Introducción y motivación](#introduccion-y-motivacion)
 * [Intento fallido](#intento-fallido)
 * [La lluvia de ideas](#la-lluvia-de-ideas)
 * [La construcción](#la-construccion)
 * [El encajado](#el-encajado)
 * [Barras y pletinas](#barras-y-pletinas)
 * [El cimbre](#el-cimbre)
 * [Haciendo los paneles](#haciendo-los-paneles)
 * [Montaje en abierto](#montaje-al-aire-libre)
 * [Comparativa al aire libre](#comparativa-al-aire-libre)
 * [Cerrando la caja](#cerrando-la-caja)
 * [Guerra de distros](#guerra-de-distros)

<h3 id="introduccion-y-motivacion"> Introducción y motivación </h3>

## La idea original

Hace unos once meses, me zambullí por primera vez en el mundillo de los SFF (factor forma pequeño) y también me hice mi primer ordenador de cero, [el cual documenté concienzudamente en inglés aquí](https://smallformfactor.net/forum/threads/playing-with-the-team-red.16087/).

La idea era conseguir un ordenador con refrigeración líquida, del "Equipo Rojo" (son AMD tanto gráfica como procesador), exclusivamente en Linux y encajando todos estos componentes

![todos estos componentes](https://i.postimg.cc/7Dh0fB5N/team-photo.jpg)

en una caja de 13 litros. La idea también era aprender sobre la marcha.

Ahora bien, como todos sabemos, un proyecto nunca está "terminado", terminado, ¿es o no?.

Me picó el gusanillo de los SFF, y sabía que se podía hacer más. En mi caso, tengo un procesador/CPU Ryzen 7 5800X y, vistas las temperaturas que cogía, lo dejé en modo ahorro de consumo, 65W, en vez de los 105W de los que normalmente es capaz.

<h3 id="intento-fallido"> Intento fallido </h3>

_En donde se expresa la frustración del SFF'eador medio, en vivas imágenes_

Una forma fácil de mejorar esta situación es aumentar el tamaño del radiador (actualmente 80mm), que realmente es de lo menos que se despacha. Teniendo en cuenta las dimensiones de la caja (2x92mm) podría uno pensar que un radiador de 2x80mm se podría usar de alguna manera. Tanto me empeñé, que hice una compra impulsiva:

![Tendría que haber mirado con más cuidado](https://i.postimg.cc/vmqJtT9r/2x80-measurements-manual.jpg)

Incluso para el radiador más pequeño que pude encontrar, la diferencia para poderlo encajar es muy grande, a pesar de la altura disponible:

![Diferencia real](https://i.postimg.cc/zGjZ30GP/2x80-real-difference.jpg)

Y, como si la caja no había tenido ya suficiente castigo, me planteé muy seriamente una modificación radical:

![Se masca la tragedia](https://i.postimg.cc/DzwVGLWf/2x80-mismatch.jpg)

... pero finalmente me decanté por echarle más horas a [Steam](https://es.wikipedia.org/wiki/Steam). Como diría Homer Simpson, este es un problema de Homer del Futuro...

... y algunos meses después, bajo un sol fantástico en cierta playa sureña, me reuní con mi gabinete técnico. En dicha reunión se me comunicó, de forma tajante, que la organización de componentes en mi ordenador habitual es subóptima. Por consiguiente, es de primera necesidad, absolutamente primordial, planificar una reforma. Se tomó nota de las varias preocupaciones, y se trazó un plan inicial de acción.

Ansina comienza la búsqueda para un nuevo hogar para mis componentes de ordenador, y por ende comienza este cuaderno de bitácora.

## Búsqueda de candidatos

Cuando decimos "subóptima", queremos decir que hay margen de mejora, queremos decir que lo queremos todo: queremos procesador a plena potencia, queremos temperaturas de la gráfica/GPU aún mejores, y queremos que todo ocupe menos litros.

Por mi parte, me las arreglé en su día para conseguir temperaturas de mi GPU [que estaban igual o por debajo de las temperaturas al aire libre](https://smallformfactor.net/forum/threads/playing-with-the-team-red.16087/post-245765), y los resultados de mis tests me revelaron que la temperatura de la GPU es probablemente la prioridad máxima.
Tras una búsqueda concienzuda, la caja [Sliger SM560](https://www.sliger.com/products/cases/sm560/) parecía ser la candidata perfecta: la posición de la GPU justo debajo de los ventiladores, también permite un radiador más grande de 120mm, mi GPU cabe, y todo esto ocupando menos litros.

La [Sliger Conswole](https://www.sliger.com/products/cases/cl530/) también era una candidata interesante, por aquello de ocupar poco espacio en la mesa, pero la refrigeración de la GPU no prometía mucho.

Al final fue necesario organizar una sesión de lluvia de ideas con mi equipo de control de calidad (que es otro equipo diferente del equipo técnico) en la cual pudiéramos ponernos de acuerdo en qué caja queremos para después, con las ideas claras, salir a buscarla.


<h3 id="la-lluvia-de-ideas"> La lluvia de ideas </h3>

_En el cual se muestran técnicas avanzadas de diseño y prototipado_

Mi configuración es particular, en el sentido de que utilizo un ladrillo AC-DC externo, lo que significa que hay espacio de 125mmx100mmx65mm que es necesario en la mayoría de cajas de ordenador, pero que yo no necesito en la mía. Este espacio se puede ocupar por un radiador. Según tengo entendido, un radiador de 140mm debería ser más que suficiente para disipar todo el calor que pueda producir mi procesador, así que opté por un [Alphacool Nexxxos XT45](https://www.alphacool.com/shop/radiators/radiators/140-mm/23676/alphacool-nexxxos-xt45-full-copper-140mm-radiator-v.2?c=20543).

Esto nos deja con tres "bloques" que se pueden reordenar dentro de una caja, como un Tangram. Nuestro equipo de ingeniería de alto estánding introdujo las dimensiones en nuestro programa de diseño último modelo:

![quien necesita CAD](https://i.postimg.cc/NMnvR2D7/components-measurements.jpg)

Puede que no tengamos una impresora 3D en nuestras instalaciones, pero tenemos cartón, corchopán, unas tijeras y muy buenas intenciones:

![maxima seriedad](https://i.postimg.cc/ZqKqc381/modelling-professional.jpg)

Podemos cortar a medida los componentes (los "bloques") en corchopán, simplemente para hacernos a la idea de los tamaños y dónde va cada conector:

![quien necesita una impresora](https://i.postimg.cc/DwcZL5Xr/modelling-components-first.jpg)

Un poco más de darle vueltas a la cabeza, y algunos bocetos aparecen:

![bocetos](https://i.postimg.cc/2yMMTdDn/modelling-drafts.jpg)

Pofesioná, muy pofesioná.
Tras jugar un poco más con los componentes, nos decidimos por algo parecido a una Sliger Conswole ([bautizada por Level1techs como Conswuela](https://www.youtube.com/watch?v=oCRHBsrz4v8)), pero con algo más de espacio para respirar, sobre todo para la gráfica. Asimismo la gráfica se situará arriba, dada la manía que tiene el aire caliente de tirar para arriba. Finalmente, este espacio extra para respirar nos permite poner unos ventiladores Noctua en el "techo de la chimenea", [algo que a nuestro equipo de ingenieros le gusta mucho hacer](https://smallformfactor.net/forum/threads/playing-with-the-team-red.16087/post-248655).

![delante](https://i.postimg.cc/kMVBzx0C/modelling-case-front.jpg)

![detras](https://i.postimg.cc/nhxrXNCV/modelling-case-back.jpg)

Este modelo pareció contentar a todo el mundo, y hubo gran jolgorio. El único detalle que faltaba era o bien encontrar una caja parecida, o bien una que pudiera ser modificada para parecerse a esta.

![medidas](https://i.postimg.cc/qRvpsTT0/panels-measurements.jpg)

Un vistazo largo a la [lista maestra de SFFs](https://smallformfactor.net/forum/threads/sff-master-list.12988/) nos revela que no vamos a encontrar nada con estas medidas. Para nuestra sorpresa, descubrimos que esto no parece nada alarmante para el gabinete técnico. Parece ser que "esto sólo es un problemilla" y "se puede solucionar fácilmente".

En lo sucesivo, se nos expone un plan que permitirá la materialización de dicha caja en aluminio, caso de trasladarnos a las instalaciones principales de la organización, donde tendremos acceso a herramientas más avanzadas.

Ni corto ni perezoso, y con plena fe en mi equipo, encargué poco después el radiador de 140mm que faltaba, el cual llegó poco antes de mis preparativos para el traslado al nuevo emplazamiento. Aquí está en toda su gloria, al lado de su homólogo de corchopán:

![ultimo componente](https://i.postimg.cc/t7Q0M0Ls/modelling-one-piece-of-the-puzzle.jpg)

Y aquí está, encajado dentro de la hipotética caja:

![ultimo componente en su sitio](https://i.postimg.cc/7Ppfk542/modelling-first-piece-in-place.jpg)

Y por último, pero no menos importante, salí a la caza de una mochila que pudiera contener justo una caja de ordenador este tamaño, caso de que existiera, para llevármela a casa (como digo, plena fe en mi equipo):

![mochila](https://i.postimg.cc/pXzyXPV1/modelling-preparing-bag.jpg)

En la foto se ve la mochila abierta, conteniendo el marco de la caja de cartón creada como modelo.

Se acabó el tiempo para ganar puntos en [Steam](https://es.wikipedia.org/wiki/Steam); llegado este punto, ya tengo totalmente desmontado mi ordenador, convenientemente empaquetados los componentes en sus cajas y estoy preparado para el traslado al siguiente emplazamiento.

¿Qué podría salir mal?


<h3 id="la-construccion"> La construcción </h3>

_En donde se relata el encuentro de nuestros héroes con taladros ancestrales, así como todo tipo de tornillos y desternillamiento_

Tenía ante mí un vuelo en avión, transportando lo que calculo deben de ser miles de euros en componentes electrónicos en mi maleta. Decidí preparame mentalmente para la travesía, a base de leer más sobre lo difíciles de encontrar que son las gráficas, y que todo el mundo quiere una y que los precios siguen subiendo. Vamos, esas cositas que ayudan a (no) tranquilizarse.

## Poniendo las cosas sobre la mesa

Terminado el vuelo y llegando a nuestras instalaciones, desempaqueto los componentes y los pongo sobre la mesa, y nos damos cuenta de la gran ventaja que supone verlos así en vivo, comparado con los modelos de cartón y corchopán.

La idea es la misma: recolocar los componentes primero, y hacer la caja después. Tal y como se ven ahora, nos damos cuenta de que podríamos haber hecho la caja más fina, por tanto más pequeña; pero pa' qué, si ya es bastante portátil. Sin duda, así tendremos más circulación de aire.

Llegados a este punto, nuestra lógica de andar por casa nos dice lo siguiente: el aire caliente se envía todo en la misma dirección y va a dar en el panel de la izquierda (eso hay que verlo), asimismo el aire caliente va hacia arriba porque es aire caliente, y porque tiene una ayudita de los ventiladores en el "techo" de la caja (eso hay que verlo). Según mi experiencia, el radiador bajo carga suelta aire bastatne templado (igual mis circuitos de agua no son óptimos), pero definitivamente el aire no es tan caliente como el de la gráfica bajo carga. Así que la gráfica, pensamos, debe ir en el piso de arriba.

![colocacion primero](https://i.postimg.cc/0N9Nqs2z/case-layout-first.jpg)

Estudiando esta imagen, nos moderamos un poco y le damos a la caja un poco más de longitud (335mm), de manera que hay un cierto margen de error para el radiador y la placa base, así como más espacio para que el aire circule. Con esta longitud, nos quedamos en **tres ventiladores** en la caja, a pesar de la gran presión ejercida por representantes de la marca Noctua dentro de nuestro proyecto para añadir un cuarto ventilador.

## Conociendo a los tornillos más de cerca

Creemos que todo esto se puede construir a base de paneles de aluminio, barras de aluminio, secciones angulares de aluminio, unas patas de plástico y tornillos. Todo se puede montar con tornillos, pero muchos tornillos. Los paneles van a ser blancos (sin pintura), pero si nos gusta cómo queda podemos hacernos de otros paneles en negro, que tengan mejor pinta. Por lo demás, el resto de materiales los encontramos en negro.

De nuestra parte tenemos todo un arsenal de herramientas de cuya antigüedad y propósito no es siempre conocido. En nuestra contra, tenemos el tiempo.


<h3 id="el-encajado"> El encajado </h3>

_En donde una caja es construida_

Lo primero es construir el receptáculo que contiene todo el volumen de la caja. Como ya hicimos un ensayo en cartón durante el prototipado, esta debería ser la tarea más golosa para empezar. La idea es la misma, pero ahora vamos a cortar aluminio en vez de cartón.

![midiendo el angulo de la esquina](https://i.postimg.cc/RVnh7dDZ/corner-measuring-angle.jpg)

¿Cómo no íbamos a tener una sierra montada para medir el ángulo de corte? Esto son las oficinas principales. Con un ángulo de 45º, todas las piezas van a encajar de manera más o menos perpendicular.

Una vez los ángulos se han cortado, vamos a demarcar con un clavo el punto donde se hará el taladro. Véase en la imagen siguiente el taladro inaugural, sujeto en su sitio gracias a una herramienta de inmenso y arcano poder:

![marca para taladro](https://i.postimg.cc/g2FJNhCq/corner-marking-for-a-hole.jpg)

Esta pieza en particular se utilizará para la bandeja de arriba, la cual es más que un "techo" ya que hospeda a los tres ventiladores de 92mm de la caja. 335mm es un poco más de la longitud necesaria para los tres ventiladores, así que usaremos algunas de las secciones negras sobrantes como relleno. La primera pieza rellena tiene esta pinta:

![relleno para los ventiladores](https://i.postimg.cc/kJ9S70hF/case-fans-fitter.jpg)

Al fondo de la foto se ven los propios ventiladores, que se usaron para la medida, demostrando que la pieza está literalmente hecha a medida.
Siempre que dos piezas se unen de manera permanente, podemos usar remaches (no hacen falta tornillos). Por supuesto tenemos en algún sitio remaches de 3 y 4mm, y una remachadora también: esto son las oficinas principales.

Para esta pieza en particular, necesitamos una pequeña esquina de aluminio que unirá las dos secciones en los dos puntos donde se ve que hay remaches.

![emplazamiento de ventiladores](https://i.postimg.cc/XqrJDkNx/case-fans-placement.jpg)

Aquí se ve el techo de la caja. Los tres ventiladores han sido lijados por un lado, del mismo modo que hice para mi caja anterior, con el objeto de pasar los cables de alimentación por debajo:

![ventilador lijado](https://i.postimg.cc/g2c1Gjmz/fan-sanded.jpg)

La base de la caja está hecha de la misma forma, más simple ya que no contendrá ventiladores. Llegados a este punto, lo que falta es añadir las secciones verticales que unirán base y techo. Una escuadrá nos ayudará a mantener el ángulo de 90º (má o meno), y, para mantener las secciones paralelas, nos sacamos de la manga el siguiente taquito de madera:

![haciendo secciones paralelas](https://i.postimg.cc/rpgwVPKQ/making-corner-sections-parallel.jpg)

Esto es muy importante, ya que a medida que vamos taladrando y poniendo tornillos, el ángulo se vuelve más difícil de corregir.

... y después de un rato muy largo, como se suele decir, [dibujamos el resto del búho](https://pics.onsizzle.com/how-to-draw-an-owl-how-to-draw-an-owl-15277527.png) ...

![caja 360x336x105 127dl rl](https://i.postimg.cc/02J39Kb4/box-360x336x105-127dl-rl.jpg)


<h3 id="barras-y-pletinas"> Barras y pletinas </h3>

El plan ahora es usar barras planas de aluminio como pletinas, y éstas ayudarán a fijar los componentes a la caja. Siguiendo en la misma línea de construir cosas a medida, las pletinas se colocan a su altura exacta y las marcas para taladro se hacen con la ayuda de la propia placa base. Véanse las pequeñas cruces a lápiz en esta foto:

![pletinas de aluminio](https://i.postimg.cc/G22hHxDr/platens-from-aluminum-flat-bars.jpg)

(Lo que no se ve en la foto son los sudores fríos mientras muevo la placa base pa'rriba y pa'bajo).

Esto marca el comienzo de más aventuras con el taladro. Para que se hagan una idea, necesitamos al menos tres pletinas en el lado derecho de la caja, dos para la placa base y al menos otra a diferente altura para el radiador. Asimismo, necesitamos otras dos pletinas en la parte de atrás, que hacen las veces de "slot" para fijar la tarjeta gráfica.

Para cada pletina necesitamos como mínimo tres taladros, así que hagan cuentas. Utilizamos un taladro montado vertical, un taladro con cable, un taladro inalámbrico cuya batería agotamos en un santiamén, y, si todo falla, tenemos **un taladro manual** (cómo no, amigos):

![taladros de todo tipo](https://i.postimg.cc/QNzVKwSz/drills-of-all-kinds.jpg)

Este termino usándolo y me termina gustando, porque resulta ser bastante práctico para las láminas finas. Hazte a un lado, Bosch.

Ahora bien, este marco ya es algo que queremos poder montar y desmontar. Una vez hecho el agujero, le aplicaremos rosca con un [tornillo autoperforante](https://i5.walmartimages.com/asr/f64a71b0-8db7-453e-9ff9-dab627ce67a9_1.77a3a52ad1bc975d41ad0aa2486d4e7b.jpeg), de manera que se pueda enroscar y desenroscar.

En la medida de lo posible, usaremos tornillos con la cabeza plana y le haremos un avellanado al agujero. Resulta que existen taladros especiales para el avellanado y ya [**teníamos uno de ellos**](https://www.dansdiscounttools.com/wp-content/uploads/2016/11/PB170002-768x768.jpg) (tenemos de todo en nuestras instalaciones).

![avellanado](https://i.postimg.cc/Jhp6nzY9/screw-countersunk.jpg)

Así que los siguientes avellanados que hagamos tendrán mejor pinta.

Quiero hacer una mención especial de la forma particularmente diabólica que hizo falta para una de las pletinas de la gráfica:

![montaje de la grafica](https://i.postimg.cc/MTKX18hC/gpu-slot-mounting.jpg)

Esta complicada sección negra, reciclada de los restos de otras secciones, lo tiene todo: el lijado para suavizar esas esquinas asesinas, y ese tornillo imposible que necesita tuerca y arandelas y algún que otro padrenuestro para que se quede en su sitio.

Habría sido mucho más fácil doblar esta parte de la tarjeta hasta dejarla paralela, pero escúchenme un momento: esta tarjeta no está modificada, se puede poner fácilmente en otra caja y también se puede cambiar fácilmente por otra tarjeta en esta misma caja. Creo que el sacrificio merece la pena.

Dándole un poco de vueltas a la caja, nos damos cuenta rápidamente de lo que será posiblemente nuestro mayor dolor de cabeza en lo que va de proyecto...


<h3 id="el-cimbre"> El cimbre </h3>

_En donde se pone a una tarjeta en su sitio_

![pletinas hechas gpu fija pero con cimbre](https://i.postimg.cc/wvGxJR5N/platens-finished-gpu-slot-fixed-but-sagging.jpg)

Siendo, como somos, bastante nuevos en este mundillo de construir ordenadores, no teníamos ni idea de lo que era el cimbre de una tarjeta gráfica. En mi caja anterior, la tarjeta estaba en vertical, [reposando sobre el lado de los tornillos](https://i.postimg.cc/qMvCcfYr/first-boot-gpu-height.jpg).

Examinando el lado suelto de la tarjeta, nos quedamos perplejos por el hecho de que no encontramos ningún tornillo que pueda ser reutilizado, ni una ranura que se pueda reutilizar para un tornillo propio. Nos preguntamos cómo ha lidiado (o no) con este problema la industria de estandardización de ordenadores. Nuestra preocupación aumenta al informarnos de otra gente con cajas "preconstruidas" enfrentándose a este mismo problema.

Después de otra lluvia de ideas y exploración de nuestras vastas oficinas, nuestros ingenieros se sacan de la manga la siguiente pieza:

![pieza anti cimbre](https://i.postimg.cc/q7rNMKQM/anti-sagging-piece-from-aluminum-bathroom-screen.jpg)

La cual creo (estoy convencido de ello) que es la pequeña idea más brillante que ha salido de este proyecto hasta ahora y seguramente de toda esta experiencia, y en tiempo récord. Cabe mencionar que esto fue aprovechado de una antigua mampara de aluminio de baño (de ahí el color blanco), cortada a medida.

Da la casualidad de que esto soluciona el cimbre tan definitivamente, que uno puede mover la caja en el aire en todas direcciones sin problemas (imagínese a mí gritando "¡cuidado!" mientras se mueve, así como limpiando mis sudores fríos).

![anti cimbre en vertical](https://i.postimg.cc/kGL4rRBC/anti-sagging-vertical-orientation.jpg)


<h3 id="haciendo-los-paneles"> Haciendo los paneles </h3>

_Que trata del recubrimiento de las impías partes íntimas de una caja, con tapas y otros retales no por ello menos pecadores_

## Preparación

![Bandeja superior](https://i.postimg.cc/qqx8tPNY/upper-tray-2mm-for-cover.jpg)

Si nos fijamos en la bandeja superior en esta foto, podemos ver unos 2 mm extra de altura marcados en verde. Estos se han dejado a posta para una posible tapa que cubriría los ventiladores. Nuestro tiempo para este proyecto es limitado, pero llegados a este punto parece que podemos permitirnos al menos unas tapas rudimentarias.

También, antes de empezar a cortar los paneles laterales, tendremos que hacer una comprobación de los ángulos de la caja, ver si realmente son ángulos rectos. Resultan no serlo totalmente:

![error de angulo en esquina](https://i.postimg.cc/YSDD0m5g/angle-errors-measuring-in-angle.jpg)

![error de angulo al final](https://i.postimg.cc/ZqnjG5Wq/angle-errors-measurings-in-tail.jpg)

Un poco de bajón, pero es bueno saberlo. Ahora el plan es hacer primero plantillas de cartulina, con la intención de que puedan ser reutilizadas más tarde para futuros paneles en negro.

Por último pero no por ello menos importante, tenemos que añadir dos pletinas en el medio para fijar el panel derecho. Si no, este panel quedaría bastante suelto por el medio (o desnudo que es peor).

![pletinas listas](https://i.postimg.cc/D0TrwvwL/platens-ready.jpg)

El panel de la derecha y el panel de arriba tendrán ambos aberturas para los ventiladores. Igual con un rectángulo valdría, pero quisimos hacerlo bien:

![plantilla para ventiladores](https://i.postimg.cc/wgNngfGf/cover-template-gpu-fans.jpg)

Las propias plantillas se utilizarán para marcar las láminas de aluminio:

![lamina marcada para cortar](https://i.postimg.cc/02ZY9qRK/sheet-marking-for-cutting.jpg)

Para dicha empresa, haremos uno de una herramienta de trazado profesional basada en grafito conocida vulgarmente como "lápiz".
Haremos uso de herramientas de similar precisión para el corte:

![cortando lamina para tapa](https://i.postimg.cc/mgDQD6BX/sheet-cutting-for-cover.jpg)

Como comentario, tengo que decir que el aluminio me ha parecido en general un material muy agradable para trabajar. Se puede doblar con relativa facilidad, se puede limar e incluso cortar cuando es fino, razonablemente robusto cuando se le cae a uno al suelo (que pasar, pasa). Terminé con polvo y virutas de aluminio por todas partes y a mi alrededor, como si de los [midiclorianos](https://starwars.fandom.com/wiki/Midi-chlorian) se tratase, y también en casi todas mis prendas de ropa. Posiblemente, se convirtió en parte de mi dieta y mi composición corporal.

Cortaremos las láminas con un poco de margen respecto a las líneas, y más tarde limaremos aquí y allá para que las formas queden más redondeadas:

![lamina cortada](https://i.postimg.cc/KcP7sM0N/sheet-cut.jpg)

Ahora también es el momento para empezar a taladrar algunos agujeros más para poder fijar los ventiladores a la bandeja de arriba.

![ventiladores vistos por debajo](https://i.postimg.cc/qByQVYv2/case-fans-below.jpg)

Podríamos hacer 12 agujeros, pero nos parece que 6 será suficiente. Seguimos quedándonos sin batería en el taladro y, pronto, sin tornillos.

## Piezas difíciles: menciones honoríficas

Llegados a este punto me siento más involucrado en el mundillo de la construcción de cajas de ordenador, y empiezo a darme cuenta de las dificultades para encontrar componentes del tamaño necesario. En nuestro caso, indudablemente, los tornillos.

Entrando en una ferretería y preguntando por tornillos de 3mm y más de 30mm de largo, puede conseguir uno que lo miren con cara de circunstancia.

Esta escena ilustra la situación perfectamente:

![tornillos complicados](https://i.postimg.cc/7ZWSTMLx/screws-challenging.jpg)

El agujero del ventilador tuvo que taladrarse una segunda vez para que fuera de 4mm. A su derecha, vemos por qué al menos la mitad de los tornillos tuvieron que ser recortados en las esquinas, unos 3mm más cortos. Y justo debajo de la esquina, vemos que hizo falta perforar un remache para que otro tornillo pudiera entrar por la derecha.

También quiero darle una mención especial al pequeño infierno que es esa tapa de detrás de la caja:

![tapa complicada para los enchufes](https://i.postimg.cc/YCbbDWqV/complicated-cover-for-connectors-side.jpg)

Y es que lo tiene todo: Esa doblez al final con ese redondeado con lima. Esos tres agujeros que tienen que coincidir perfectamente con las roscas de debajo, y qué me dicen de ese cambio de anchura a mitad de trayecto...

## Retoques finales

Ahora tenemos una idea bastante clara del espacio que nos queda, para taladrar todavía otros cuatro agujeros. Es un buen momento para ponerle las patas a la caja.

La solución será un poco más rápida e improvisada, aunque tenemos ya las patas de plástico. Los ingredientes son cuatro tacos de plástico para pared, las cuatro patas y por supuesto tornillos. El resultado tiene exactamente esta pinta:

![solucion para las patas](https://i.postimg.cc/KjSN37JK/feet-solution-big-holes-and-screw-anchors.jpg)

A estas alturas, empezamos a ir mal de tiempo, y la administración central empieza a ponerse impaciente y a hacer preguntas. Por lo tanto, nos preparamos para un montaje rápido de la caja. Seguramente no llegaremos a encender el ordenador, pero en el tiempo que queda, deberíamos al menos encontrar un sitio para poner el botón de encendido, y también rellenar el circuito de agua.

Me concentro primero en el circuito. No tengo ni idea de cuál es la política del aeropuerto en cuanto al control de equipaje de mano y circuitos de agua para ordenador, con todo ese líquido saliendo en el escáner y todo eso. Y tampoco tengo tiempo para hacer averiguaciones, así que me decido a no llevarlo en la mochila. Creo que tengo tiempo suficiente para montarlo, llenarlo y empaquetarlo por separado.

Ya tenemos algunas tablas con circuitos de agua, y este fue bastante fácil en comparación. Quizá sea el tubo izquierdo, que tiene un tramo más largo, se podría haber dividido en dos secciones con racor o un muelle anti-dobleces, y seguramente lo haga si se da el caso de tener que vaciarlo otra vez. Este golpe, me pasé de listo y añadí un conector (hembra-hembra) con un sensor de temperatura (cuadrado en el medio):

![circuito vacio para medidas](https://i.postimg.cc/2yQxQqK0/empty-loop-for-measurements.jpg)

Me pareció una forma de ser precavido y resultó todo lo contrario. El observador experimentado podrá reconocer rápidamente el problema de este montaje: esta placa base no tiene un conector para sensores de temperatura. Lo cual descubrí justo antes de montar el circuito, y fue una chafada importante.

También este golpe me hice de una llave Allen, para abrir la tapa hexagonal que tiene la bomba Eisbaer (marcada en verde), de manera que hay dos puntos que se pueden usar para llenar el circuito de agua.

![puntos de llenado](https://i.postimg.cc/nLNKNY8d/loop-fill-points.jpg)

El punto marcado en rojo se puede abrir como entrada de aire. Moviendo el circuito en varias direcciones, da la sensación de no tener burbujas, de estar lleno. Sin embargo, podemos seguir llenando por el lado de la bomba. La explicación es que el radiador es muy fácil de llenar por la forma que tiene, pero la bomba no tanto. También nos sorprende la capacidad del radiador: faltó poco para quedarnos sin líquido.

Ahora, el botón de encendido. Esto debería ser relativamente simple, nos basta con saber el diámetro del taladro que necesitamos. Como vemos, tenemos también la opción de ponerlo detrás en caso de que la parte frontal se llene de cables:

![boton de encendido](https://i.postimg.cc/MTR0wMqz/power-button-correction.jpg)

¡Y mira qué cosita!¡Ay, qué cosita más bonita!

![paneles blancos en ventiladores](https://i.postimg.cc/50FgtKqd/covers-white-on-fans.jpg)

Ahora viene la prueba re-refinitiva. ¿Estuvimos finos cuando medimos la mochila con nuestro prototipo de cartón?

![mochila prueba real](https://i.postimg.cc/XY7g5Ycg/carrying-bag-real-test.jpg)

Sí señor. [Me encanta que los planes salgan bien](https://images6.fanpop.com/image/photos/37200000/-John-Hannibal-Smith-the-a-team-37241333-400-294.jpg).

Estoy orgulloso de este equipo y de lo que hemos conseguido. Ahora, toca un muy merecido banquete, y descanso.
Después, un delicado viaje, transportando el material de alta tecnología.


<h3 id="montaje-al-aire-libre"> Montaje en abierto </h3>

_En donde presentamos al temido enemigo a batir, el montaje al aire libre_

Después de un viaje sin sobresaltos (excepto la cara de la empleada que pasaba mi mochila por el escáner en el aeropuerto, que fue un poema), y después de ir asentándome, empecé a tener tiempo para desempaquetar y sacar el circuito de agua de su caja, e instalarlo en la caja que había traído en la mochila.

![filled loop](https://i.postimg.cc/pVFZgvvb/filled-loop.jpg)

Nunca me terminó de convencer el hecho de que, durante mis pruebas, el aire caliente que salía del radiador nunca se sentía tan caliente como el que expulsaba la gráfica. Este era el caso, incluso cuando los sensores de temperatura están claramente indicando que el procesador se está tostando. Por eso, he tenido cada vez más cuidado rellenando el circuito de agua, no dejando burbujas de aire dentro, y este golpe también voy a buscarme una pasta térmica de más calidad:

![loop filled mounting](https://i.postimg.cc/dtcBFfQ7/loop-filled-mounting.jpg)

... A ver si se nota en algo. También he conseguido pequeñas mejorías en temperaturas aquí y allá, a base de limpiar bien la superficie de la bomba y distribuir mejor la pasta.

Si lo pensamos bien, hemos partido de una suposición bastante atrevida sobre esta caja, asumiendo que la ventilación será mejor y mejor refrigeración de la gráfica. **Tan atrevida, que nos echamos pa'lante e hicimos la caja**.
Además, le puse al procesador un radiador de 140 mm, comparado con el anterior de 80 mm, así que no estaría de más ver alguna mejoría en las temperaturas del procesador.

Ya que hemos llegado a este punto, opinamos que lo primero es que el "producto" final, cerrado y con sus tres ventiladores, obtenga temperaturas más bajas que dejando los componentes al aire libre. En otras palabras, que "caja" sea mejor que "no caja".

Más adelante podemos hacer otras comparativas, como por ejemplo retocar las curvas de potencia de los ventiladores, comparar resultados con los obtenidos en la caja anterior, etc. Pero todo ello, tomará este montaje al aire libre como referencia.

Para esto necesitaremos, primeramente, un montaje que tiene exactamente esta pinta:

![benchmarking open air left](https://i.postimg.cc/BbHBDTpQ/benchmarking-open-air-left.jpg)

![benchmarking open air right](https://i.postimg.cc/76pNfsgW/benchmarking-open-air-right.jpg)

... Y segundo, pero no por ello menos importante, un conjunto de tests que pueda usarse como referencia.


<h3 id="comparativa-al-aire-libre"> Comparativa al aire libre </h3>

Ahora por fin empezamos a meternos en mi terreno, que es el software. Estamos al fin y al cabo en Helsinki, una ciudad donde los videojuegos tienen cierta relevancia...

![gaming in helsinki](https://i.postimg.cc/nzskVbQm/gaming-in-helsinki.jpg)

Y ahora, déjenme explicar mi problema con las comparativas. Siempre que hay un descuento en la tienda de juegos, veo también un buen programa de tests como 3DMark en oferta. Creedme, lo veo. Pero también veo algún juego, bastante bueno, en oferta. Entonces ¿qué hago?

Muy a menudo, el juego se puede usar también como test en una comparativa. Y además, se puede jugar. Y es un experimento que se acerca mucho más al uso real del ordenador.

De cualquier manera, esta es mi excusa para sacarme de la manga la siguiente batería de tests sorprendente, perturbadora, e improbable:

- [Build GCC](https://openbenchmarking.org/test/pts/build-gcc): Nunca falla, tuesta mi procesador y alcanza las máximas temperaturas incluso en modo ahorro de energía.
- [Unigine superposition](https://openbenchmarking.org/test/pts/unigine-super): Nunca falla, tuesta mi gráfica, y sólo he conseguido bajar de la temperatura máxima aumentando considerablemente la velocidad de los ventiladores.
- [Shadow of the Tomb Raider](https://store.steampowered.com/app/750920/): Este juego se ha convertido en una cara familiar en la mayoría de presentaciones de nuevos procesadores y gráficas.
- [Total War Warhammer II](https://store.steampowered.com/app/594570/total_war_warhammer_ii/): Batallas grandes, muchos efectos especiales, resultados muy fiables, juego tremendamente popular.
- [The Riftbreaker](https://store.steampowered.com/app/780310/The_Riftbreaker/): Un juego del 2021 que es una fiesta de explosiones y partes corporales, y una gran prueba para el procesador.
- [Dawn of War 3](https://store.steampowered.com/app/285190/): Muchas explosiones, y posiblemente el test más fiable de la lista. Nunca se atasca, nunca necesita repetición.
- [Company of Heroes 2](https://store.steampowered.com/app/231430/): Para cualquiera que proteste de que el juego anterior no es popular, este es muy parecido y tiene miles de jugadores.
- [Ashes of the Singularity: Escalation](https://store.steampowered.com/app/507490/Ashes_of_the_Singularity_Escalation): Muchísimas explosiones. Solía ser la niña bonita de DirectX12, aunque a día de hoy necesita muchos reintentos para ser fiable.

_O sea, que mi criterio para elegir un test se reduce a explosiones._

Antes de desmontar mi caja anterior, fui lo suficientemente precavido como para automatizar la secuencia de tests ya mencionada. Hice una actualización general de todo el software, y conseguí resultados para los dos sistemas instalados (SalientOS y Garuda). Guardé estos resultados en un disco externo, de manera que ahora puedo incluirlos en la comparativa.

¿Estamos listos ya? Todavía no, necesitamos también un nombre para los resultados de esta comparativa. Lo que significa que debería inventarme un nombre para esta caja.
La voy a llamar **"Big Conswela" (Consuela grande)**, quizá por aquello de que es una caja tipo consola, pero el tamaño es un poco mayor de lo habitual. De todas formas, estoy abierto a sugerencias.

Y ahora ya sí, puedo ejecutar los tests:

![benchmarking opencase summary](https://i.postimg.cc/8Cz43rpt/benchmarking-opencase-summary.jpg)

En los resultados, vemos el típico toma y daca entre Salient y Garuda. También vemos lo que puede ser el efecto de una actualización a Linux 5.15.12. En cualquier caso, muchas victorias (en verde) para Conswela. ¿A qué podría deberse? Los componentes son los mismos y el procesador sigue en modo ahorro. ¿Podría ser por mejor refrigeración?

Ahora, echemos un vistazo a la parte que quizá sea lo más importante por ahora, la tostadora de procesadores:

![benchmarking open air cpu temps](https://i.postimg.cc/yY0LSHpX/benchmarking-open-air-cpu-temps.png)

Aquí vemos una mejora en las temperaturas, y, por primera vez, veo la temperatura máxima bajar de los 80 grados. La temperatura media bajó 6 grados, de 69 a 63. Está bien, pero un poco decepcionante. Creo que no desactivaría el modo ahorro solo por esos 6 grados.

Ahora, veamos qué pasa con la tostadora de gráficas:

![gpu max power consumption](https://i.postimg.cc/SK1dpNjq/gpu-max-power-consumption.png)

Hambrienta como siempre, obligando a la gráfica a consumir 240 vatios independientemente del sistema operativo, o la caja.

![benchmarking open gpu temperatures](https://i.postimg.cc/mgwV8SsP/benchmarking-open-gpu-temperatures.png)

Las temperaturas son un poquito mejores al aire libre que en la caja anterior. Esto significa que se me olvidó activar el control de ventiladores cuando hice esas medidas. Los ventiladores deberían haber reaccionado a la temperatura, y los resultados habrían sido más similares.

Por último, no nos olvidemos de que estamos utilizando un sistema de archivos con copia en escritura (BTRFS), con funcionalidades muy interesantes, pero que sube la temperatura del disco duro una barbaridad. Para muestra, un botón:

![benchmarking open air nvme temps](https://i.postimg.cc/nhZ04P68/benchmarking-open-air-nvme-temps.png)

En primer lugar, las temperaturas son más altas en Garuda (usa BTRFS) que en Salient (que usa ext4), pero ambos estaban usando la caja anterior, en la cual los ventiladores de caja están gobernados directamente por la temperatura del procesador. Como resultado, durante este test, la ventilación era mucho mejor en la caja anterior que al aire libre, como se demuestra aquí.
Esto también nos sirve para ilustrar por qué hay que tener cuidado con sistemas como BTRFS, cuando no tenemos buena refrigeración.


<h3 id="cerrando-la-caja"> Cerrando la caja </h3>

_En donde se relatarán hallazgos verdaderos_

Año nuevo, caja nueva. Me hace ilusión ponerla a prueba, pero tengo mis reparos. Me doy cuenta de que me he involucrado demasiado en su construcción, y ahora no quiero ver resultados negativos.

La realidad tiene esa cualidad especial para que las cosas no hagan lo que esperamos que hagan... y ya ni hablamos de lo que **queremos** que hagan.

Recordemos, por ejemplo, el asunto de los tornillos de dimensiones difíciles. Obsérvese una nueva adición a nuestro zoológico de tornillos: el tornillo plano, de base plana y 35 mm:

![fans piece mounting](https://i.postimg.cc/rs15dkFF/fans-piece-mounting.jpg)

¿Y qué decir del cable de alimentación que es demasiado ancho para las patas, y nunca te diste cuenta hasta que terminaste la caja?

![supply cable orientation limited](https://i.postimg.cc/7Zs0JXD7/supply-cable-orientation-limited.jpg)

Afortunadamente, tenemos opciones. Podemos dejarlo así

![supply cable orientation right](https://i.postimg.cc/zXkn88cx/supply-cable-orientation-right.jpg)

... O asá:

![supply cable orientation left](https://i.postimg.cc/bv5xWgB8/supply-cable-orientation-left.jpg)

Me decido por el lado izquierdo, ya que parece dejar más espacio para liberar la mesa de cables. Quizás, incluso una pequeña bandeja bajo la mesa, donde iría el ladrillo. He hecho exactamente lo mismo para el ladrillo del portátil.

Siguiendo muy en la línea de este proyecto, durante el atornillado y destornillado de uno de los paneles, recurrí al ingenio siguiente para recordar dónde va cada tornillo:

![how to remember screw position](https://i.postimg.cc/NjfR0ZhB/how-to-remember-screw-position.jpg)

Por fin, ha llegado la hora de pensar en la refrigeración. Hay varios caminos que nos pueden llevar a Roma, pero me decido por este:

![chassis fans connected](https://i.postimg.cc/VNJMqMLh/chassis-fans-connected.jpg)

Esta vez, conectaré el ventilador del radiador con el inmediatamente superior (en rojo), lo cual significa que ambos están regulados por la temperatura del procesador. Los dos ventiladores restantes (en azul) serán ventiladores de caja. Estando tan cerca de la gráfica, me parece una buena idea hacerlos reaccionar a la temperatura de esta.

![chassis fans distribution cpu gpu](https://i.postimg.cc/6QFZY38p/chassis-fan-distribution-cpu-gpu-gpu.jpg)

Equipado con mi siempre presente destornillador y mi huevera, ha llegado el momento de echar un primer vistazo al fantástico artefacto:

![closed right](https://i.postimg.cc/6qJd53Km/closed-right.jpg)

![closed left](https://i.postimg.cc/nVB4SvYb/closed-left.jpg)

En cuestión de minutos, lo enciendo y organizo la ventilación, de manera que los dos ventiladores arriba mencionados respondan a la temperatura de la gráfica. Esta es prácticamente idéntica a la configuración "fantcl" que he utilizado anteriormente, con la única diferencia de que ahora tenemos solo un ventilador del lado del procesador.

Y bueno, tengo que dar algunas explicaciones... sobre esta primera prueba. Este número en general no me sirve, pero lo voy a mostrar como ejemplo ilustrativo:

![closed fanctl gpu temps](https://i.postimg.cc/MK0YHyvb/closed-fanctl-gpu-temps.jpg)

Aquí se ve al bueno de Unigine, haciendo lo que sabe hacer, consumiendo sus 240 W y las temperaturas de la gráfica parecen haber mejorado respecto al aire libre, y también respecto a la caja anterior. Hasta aquí todo bien, hasta que echo un vistazo a la izquierda de la caja y me doy cuenta de que los ventiladores de la gráfica no giran.

Como la comparativa ha terminado, lanzo un juego y vuelvo a mirar a la gráfica. Sigue parada.

Ahora lanzo el test de Tomb Raider, que es más exigente. Espero un poco. **La gráfica sigue parada**.

Empiezo a preocuparme bastante, pero esta vez lanzo directamente el Unigine. Espero a que se caliente un poco y sí, los ventiladores de la gráfica empiezan a moverse lentamente, tímidamente.

En definitiva: lo que _creo_ que ha pasado aquí es que esos dos ventiladores de arriba están refrigerando ya de por sí lo suficiente, hasta el punto de que el regulador de la gráfica no considera necesario encender **sus propios ventiladores**.

En cualquier caso, esto no nos va a valer. Cuando estuvimos planeando esta caja, pensamos en un "túnel de aire" o más bien una pared de aire, que funciona de la siguiente forma:

![closed airflow idea](https://i.postimg.cc/xCbv2mwm/closed-airflow-idea.jpg)

_Aire frío en azul, entrando. Aire caliente en rojo, saliendo_

La idea es que queremos ver esos ventiladores girando, al menos un poquito. Así que vamos a hacer un poco de trampa, y ajustarle los valores a la gráfica, de manera que estos girarán aunque sea silenciosamente, simplemente para que el aire circule mejor.

A esta configuración la voy a bautizar como **fangpu** para diferenciarla de la anterior "fanctl". Hago la prueba otra vez, suma y sigue:

![closed fangpu temps](https://i.postimg.cc/jSs47LfV/closed-fangpu-temps.jpg)

Vale, esto son temperaturas muy bajas, esto ya es una exageración. Pero lo que realmente importa es que las temperaturas del procesador también han bajado, aunque sea muy, muy tímidamente:

![closed fangpu cpu temps](https://i.postimg.cc/fTQKMHTR/closed-fangpu-cpu-temps.jpg)

La prioridad de esta caja era optimizar la circulación de aire en la gráfica (cuidado con lo que deseas), y al procesador lo di por sentado, pensando que el radiador iba a solucionarlo todo. Ahora me doy cuenta de que el procesador va a ser el auténtico cuello de botella. Tal y como yo lo veo, el siguiente paso debe ser dedicarle los **tres ventiladores** de la caja al procesador, configuración que bautizaré con el poco original nombre de **fancpu**.

Para ser más claros: no cambiamos nada más, solamente dedicamos al procesador estos dos ventiladores en azul:

![closed fanctl fans](https://i.postimg.cc/1zX0v2MJ/closed-fanctl-fans.jpg)

...reaccionando a la temperatura del procesador, tal y como hacen los marcados en rojo. Este será el único cambio y aun así, parece que hemos batido un buen puñado de récords de rendimiento:

![closed fancpu marks](https://i.postimg.cc/T2D0M8xs/closed-fancpu-marks.jpg)

Lo cual me convence aún más de mi hipótesis de que el procesador es el factor limitante.

No tengo problema en admitir que estos números no son espectaculares. No me imagino dopando este procesador (si supiera cómo se hace) y ciertamente la gráfica está tan fría, que hay margen para subirle la potencia (si supiera cómo).

Lo que sí que importa es que, llegados a este punto, **hemos demostrado una mejoría en prácticamente todos los aspectos**.

![closed fancpu improvement](https://i.postimg.cc/wvSky2XF/closed-fancpu-improvement.jpg)

Esto son 10 grados como 10 soles por debajo de mi montaje anterior. Y del orden de 2-3 grados por debajo del montaje al aire libre.

Además, hemos reducido **unos 15 grados muy respetables en el disco duro**:

![closed fancpu ssd temps](https://i.postimg.cc/Xqdkw5xW/closed-fancpu-ssd-temps.jpg)

Curiosamente, este es el único aspecto en el que la caja anterior mantiene una ventaja.

![big1 cpu vents](https://i.postimg.cc/pT9ZL4Fp/big1-cpu-vent.png)

Sería un experimento interesante, ver qué pasa abriendo unas ranuras de ventilación en frente de la placa base, como en la caja anterior. Pero, como diría Michael Ende: esa ya es otra historia, que debe ser contada en otra ocasión.

Llegados a este punto tengo una _vaga idea_ de cómo de bien rinde mi caja. La vida sería muy fácil y estaría todo muy bien, si los ingenieros nos conformáramos con una _vaga idea_. En los días siguientes, procederé a demostrar, con pruebas sistemáticas y fiables, que estas mejorías persisten a lo largo de multitud de intentos de test, y a través de diferentes sistemas operativos.


<h3 id="guerra-de-distros"> Guerra de distros </h3>

_En donde los sistemas operativos luchan por nuestra causa_

Todo el mundo sabe que la forma más razonable de realizar comparativas es repetir tus medidas en cuatro sistemas operativos diferentes... como mínimo.

Hace 11 meses desde mi último montaje, y la inflación e impedimentos logísticos han limitado las buenas noticias por parte del hardware. Sin embargo, en el mundo del software han pasado un montón de cosas. Y, para ser más precisos, en el mundo de Linux han pasado un montón de cosas.

A Tesla le dio por crear puestos de trabajo [para juegos en Linux](https://mobile.twitter.com/attlka/status/1397258716076937216).

Ni más ni menos que Valve (la empresa de Steam) anunció una [nueva consola](https://www.steamdeck.com/en/) que estaría basada en Linux como sistema operativo principal.

Esta agitación fue más que suficiente para que varios youtubers/influencers le dieran a Linux una oportunidad.

Como mención especial, Linus (el chillón del canal de LTT, no el autor de Linux) el cual se propuso trabajar y jugar exclusivamente en Linux de ahora en adelante, y se lo propuso como un reto, encontrándose con tremendos problemas en más de una ocasión.

Como resultado, muchas de las distribuciones de Linux (distros) han pasado a ser el centro de atención, y se han dado prisa en cubrir sus vergüenzas a lo largo del año pasado:

- La propia Valve [puso a Manjaro en el candelero](https://partner.steamgames.com/doc/steamdeck/testing), al recomendarla como la mejor opción para testear su futura consola.
- System76 solucionó [una dependencia con muy mala pinta](https://www.gamingonlinux.com/2021/11/system76-patches-apt-for-pop-os-to-prevent-users-breaking-their-systems/) a raíz de su (muy desafortunada) aparición en el show de LTT.
- Canonical (Ubuntu) creó [un nuevo puesto de trabajo específicamente para videojuegos](https://canonical.com/careers/3776036).

Para este golpe, estoy decidido a probar la distribución de Intel, [**Clear Linux**](https://clearlinux.org/). Primeramente, porque siempre he tenido curiosidad, y, además, porque [parece que planean alejarse poco a poco del usuario de escritorio](https://www.phoronix.com/scan.php?page=news_item&px=Clear-Linux-Divesting-Desktop). Entonces, para mí, era una cuestión de ahora o nunca.
La distribución de Intel [tiene una reputación de no funcionar con mi preciso modelo de tarjeta gráfica](https://community.clearlinux.org/t/radeon-6000-series-help/5969). Vale, gracias, díganme qué otra cosa no puedo hacer:

![clear linux installing](https://i.postimg.cc/vTcF7LPW/clear-linux-installing.jpg)

![clear linux tested](https://i.postimg.cc/HnRG6cKF/clear-linux-tested.png)

En mis primeras pruebas, me topo con algunos tests que no funcionan. Esto no es una gran sorpresa, considerando que esta distribución está más especializada en funcionar en la nube, y no tanto en un ordenador personal:

![clear linux results](https://i.postimg.cc/qv8fG7GZ/clear-linux-results.png)

En todo caso, es una sorpresa encontrarse con que la mayoría de tests funcionan sin necesidad de chanchullos. No voy a invertir tiempo en solucionar problemas con los que no funcionan... puede que luego, si me decido a probarla otra vez.

Como comentario general, me gustan estos resultados. El rendimiento en juegos con DX11/DX12 es bastante satisfactorio, y el rendimiento de juegos nativos es simplemente impresionante. Buen trabajo por parte del equipo de código abierto de Intel: sea cual sea el tipo de conjuro que hayan usado, funciona... y me imagino que nunca se imaginaron que alguien intentaría instalar Tomb Raider en su sistema operativo.

A raíz de mi montaje anterior, recibí algunos comentarios relacionados con mi elección de sistemas operativos muy minoritarios. Esta vez, voy a cambiar la alineación un poquito, para que incluya algunos pesos pesados (es decir, si la propia distribución de Intel no parece ya lo suficientemente relevante):

- Sube al ring, el peso pesado número, con [más de 90 mil paquetes](https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions#Package_management_and_installation)... [**Fedora 35**!](https://getfedora.org/), también conocida como **la distro de coña**.

![fedora welcome](https://i.postimg.cc/MpJNXS80/fedora-welcome.jpg)

La instalación va como la seda, y creo que me está haciendo un pequeño guiño al mostrarme la marca de mi placa base durante la pantalla de inicio... quizá Fedora es capaz de reconocer el fabricante de alguna forma.

![fedora recognizes asrock](https://i.postimg.cc/PJBcWGvj/fedora-recognizes-asrock.jpg)

Es la primera vez que veo algo así, pero al mismo tiempo no me sorprende. Este es el nivel de detalle que me espero de la niña de los ojos de IBM/Red Hat. Fedora es el sistema operativo que nos muestra lo que se convertirá en tendencia en el mundo de Linux en los años venideros. [¿Os parece bastante?](https://www.youtube.com/watch?v=rHpWKPfvTmM&t=522s)

Que me detengan:

![fedora uncensored](https://i.postimg.cc/t4dfxMXK/fedora-uncensored.jpg)

Tras pasar a Fedora por la batería de tests habitual, obtengo los siguientes resultados:

![fedora results](https://i.postimg.cc/GprWg173/fedora-results.png)

... Y, mira por dónde, batió algún que otro récord. No sin algún "pero":

- El gestor del procesador ("cpufreq") estaba en modo ahorro de energía, por defecto. Tuve que buscar la documentación de Fedora para cambiarlo a modo "**rendimiento**". Como resultado, el rendimiento mejoró, razón por la cual añadí "cpufreq" al nombre de esta tanda de tests.

- Los juegos en DX11/DX12 corren más lentos: una reducción de más o menos 10 FPS o fotogramas por segundo, respecto a otras distribuciones. Y eso, después de haber puesto el gestor en modo "rendimiento".
- Uno de los juegos no funcionó.

El punto fuerte de Fedora está en las tareas en Linux nativo, como la compilación. El rendimiento del procesador mejoró, pero trajo consigo una subida de un grado en temperatura:

![fedora cpu temps](https://i.postimg.cc/mZ5GLJDV/fedora-cpu-temps.png)

- Ahora, permítanme echar Fedora a un lado, e introducir al peso pesado número dos. [Con más de 125 mil paquetes a su nombre](https://en.wikipedia.org/wiki/Comparison_of_Linux_distributions#Package_management_and_installation), ese otro sistema operativo del que quizá no hayan oído hablar, llamado [**Ubuntu 21**](https://ubuntu.com/) Impish Indri; Un pícaro animal ([el Indri](https://es.wikipedia.org/wiki/Indri_indri)) que hará su aparición inmediatamente durante el proceso de instalación:

![ubuntu installation](https://i.postimg.cc/FFq25SW-B/ubuntu-installation.jpg)

Con una pinta de Impío, el Indri...

Mucha gente usa Ubuntu, y es que hay motivos. El proceso de instalación es tan sencillo que tiene menos pasos que mi última instalación de Windows 10. Para mí, a día de hoy, Ubuntu es el sistema a prueba de torpes, yo incluido.

Para empezar, todos los tests funcionaron. El Indri Impío se afanó unos cuantos récords de Fedora:

![ubuntu benchmark results](https://i.postimg.cc/C1r9KnFd/ubuntu-benchmark-results.png)

Cabe destacar, a estas alturas de la película, lo improbable que es que se bata un récord en algún caso.

Los resultados en juegos de DirectX son un poco mejores, pero no los mejores que he visto. Quiero hacer hincapié en juegos de DirectX11/DirectX12 porque, a la hora de la verdad, son los más populares para el público en general.

Esta vez, no hemos pagado ningún precio en cuanto a temperatura del procesador, y el rendimiento fue incluso mejor:

![ubuntu cpu temps](https://i.postimg.cc/LsTrr0wt/ubuntu-cpu-temps.png)

En resumen, muchos resultados muy positivos.


## Las secuelas de la batalla

Mi pequeño _tour_ por las distribuciones de Linux me ha reportado una buena cantidad de datos. Mis conclusiones son principalmente positivas:

- La mejoría que hemos visto en las temperaturas de la gráfica con esta caja se ha mantenido estable, a lo largo de todos los sistemas operativos.

Unigine solía ser un test terrorífico: el aire salía ardiendo del receptáculo de la gráfica, los ventiladores iban a toda pastilla, se oía un quejido en la bobina de la fuente de alimentación. Sin embargo, ¿ahora?, es casi un test de coña:

![ubuntu gpu temps](https://i.postimg.cc/Kz8dFDhd/ubuntu-gpu-temps.png)

- Hablando de temperaturas, Fedora 35 puede presumir de las mejores temperaturas de un disco duro que hemos visto hasta la fecha, y todo esto en un disco sobre BTRFS:

![distro wars drive temps](https://i.postimg.cc/Nf2v5D2t/distro-wars-drive-temps.png)

- En un mundo donde el marketing intenta convencernos de que "cada FPS cuenta", alguna gente se gasta unos pocos cientos de euros extra en una tarjeta gráfica, solo para conseguir un incremento marginal en rendimiento (FPS). Me pareció que sería una buena idea generar una media geométrica de mis resultados en cuanto a FPS:

![distro wars fps mean](https://i.postimg.cc/7Zv87zS2/distro-wars-fps-mean.png)

La distribución que destaca (que sigue destacando) para mí es Garuda. La [distro del águila](https://garudalinux.org/) no bate muchos récords, pero sí que consigue muchas segundas posiciones en los ránkings, de manera que consigue la primera posición en la media. A esto debemos añadir: la tremenda facilidad para actualizar en Garuda, las copias de seguridad automáticas tomadas en momentos clave, y la posibilidad de "retroceder en el tiempo" si rompemos algo, la estrategia de configurar todo con ventanas de manera que cambiar el kernel es solo hacer clic en una casilla... y creo que tenemos una ganadora.

Dicho esto, vemos diferencias de rendimiento muy limitadas entre las distribuciones. Creo que tiene mucho más impacto la capacidad de refrigeración del procesador, y, por tanto, es una pista de que podemos conseguir mucho más impacto si jugamos con la potencia/voltaje/frecuencia del procesador.

Las mejoras en refrigeración de la gráfica no trajeron consigo más rendimiento, lo cual no quita que no podamos comprobar qué pasa cuando reducimos el voltaje o incrementamos la frecuencia, ya que las bajas temperaturas nos lo permiten.
